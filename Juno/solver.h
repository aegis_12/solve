
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#if defined(__CUDA_ARCH__) || defined(__CUDACC__)
#define __cudaFunction__ __device__
#ifndef __real__
#define __real__ float
#define absolute fabsf
#endif
#else
#define __cudaFunction__
#ifndef __real__
#define __real__ double
#define absolute fabs
#endif
#endif

/**
*	Arriba tambien se ha definido el absoluto como el valor absoluto.
*	Segun el tipo de valor (float o double) se tendrá que usar una u otra
*/

#define RESIDUAL 1
#define JACOBIAN 2

#define real __real__

#define min(a,b) ((a) <= (b) ? (a) : (b))
#define max(a,b) ((a) >= (b) ? (a) : (b))
#define TRUE (1)
#define FALSE (0)

/**
*	Aunque la funcion tiene una directiva/pragma __cudaFunction__, parece que no quiere que lo poonga esa directiva aqui
*	Esta es la referencia que se usa internamente dentro de las funciones del levmar para hacer referencia a la funcion
*	del modelo (residual y jacobiano)
*/
typedef void (*func_decl)(real *x, real *xdata, real *ydata, int nObs, int nPrm, real *fvec, real *fjac, int flag);

__cudaFunction__
void modelo(real *x, real *xdata, real *ydata, int nObs, int nPrm, real *fvec, real *fjac, int flag);

__cudaFunction__
void initialize_params(real *x);

// extern const int numParams;

#ifdef __cplusplus
}
#endif /* __cplusplus */
