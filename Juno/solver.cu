
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <nifti1_io.h>
#include <cstdlib>
#include "solver.h"

#define NUM_MAX_NOBS 30

/**
*	Implementacion de la norma para evitar workflows
*	Basado en el programa dnrm2 del lapack
*/

__cudaFunction__
real norm(real *x, int nObs)
{
	long int ix, nn, iincx;
	real norm, scale, absxi, ssq, temp;

	nn = nObs;
	iincx = 1;

	if( nn > 0 && iincx > 0 )
	{
    	if (nn == 1)
    	{
      		norm = fabs(x[0]);
    	}  
    	else
    	{
      		scale = 0.0;
      		ssq = 1.0;

      		for (ix=(nn-1)*iincx; ix>=0; ix-=iincx)
      		{
        		if (x[ix] != 0.0)
        		{
          			absxi = fabs(x[ix]);
          			if (scale < absxi)
          			{
            			temp = scale / absxi;
            			ssq = ssq * (temp * temp) + 1.0;
            			scale = absxi;
          			}
          			else
          			{
            			temp = absxi / scale;
            			ssq += temp * temp;
          			}
        		}
      		}
      		norm = scale * sqrt(ssq);
    	}
  }
  else
  	norm = 0.0;

  return norm;

}

__cudaFunction__
void printVector(real *v, int n)
{
	int i;

	for (i=0; i<n; i++)
		printf("%f ", v[i]);

	printf("\n");
}

__cudaFunction__
void printMatrix(real *v, int m, int n)
{
	int i, j;
	for (i=0; i<m; i++){
		for (j=0; j<n; j++){
			printf("%f ", v[i+m*j]);
		}
		printf("\n");
	}
}

/**
* Devuelve beta y se sobreescribe el vector v
* x es una de las columnas de la matriz. N es el tamaño
* de ese 'vector'
*/
__cudaFunction__
real householder_vector(real *v, real *xA, int n) 
{
	
	int i;
	real beta = 0.0, alpha=0, uta=0, xAux;

	real xNorm = norm(xA, n);

	v[0] = 1;

	for (i=0; i<n; i++)
		v[i] = xA[i] / xNorm;

	real xFirst = v[0];

	/* Calcular el valor de alpha x(2:n)' * x(2:n) */
	for (i=1; i<n; i++)
	{
		alpha += v[i] * v[i];
	}

	if (alpha == 0)
	{
		return 0;
	}
	else 
	{
		uta = sqrt(xFirst*xFirst + alpha);

		if (xFirst <= 0){
			v[0] = xFirst - uta;
		} 
		else {
			v[0] = -alpha / (xFirst+uta);
		}

		beta = 2 * (v[0]*v[0]) / (alpha + v[0]*v[0]);
		xAux = v[0];

		/* Update v value */
		for (i=0; i<n; i++)
			v[i] = v[i] / xAux;
	}

	return beta;
}

/**
*	Cuidado. Esta funcion no se puede pasar a otro lado, es solo para el levmar
*	Tiene algunos cambios adhoc. El fvec que es la última columna, tiene el signo cambiado
*/
__cudaFunction__
void qrSolver(real *A, real *fjac, real *fvec, int m, int n, real *step, real lambda, real *vAux, real *v) 
{
	int i, j, c, d, k;

	real aux, beta;

	/**
	*	Hacer la copia de fjac y fvec a la matriz auxiliar A
	*	que será la que se sobreescribira
	*/

	int lmit = m-n+1;

	for (i=0; i<n-1; i++){
		for (j=0; j<lmit; j++){
			A[i*m+j] = fjac[i*lmit+j];
		}
	}

	for (i=0; i<lmit; i++){
		A[m*(n-1)+i] = -fvec[i];
	}

	for (i=lmit; i<m; i++){
		A[m*(n-1)+i] = 0;
	}

	for (i=0; i<n-1; i++){
		for (j=0; j<n-1; j++){
			A[i*m+lmit+j] = 0.0;
		}
		A[i*m+lmit+i] = sqrt(lambda);
	}

	/**
	*	Usar las reflexiones de householder para eliminar valores
	*	por debajo de la diagonal. Por ahora se usa un valor auxiliar vAux
	*	para hacer la multiplicacion
	*/

	for (i=0; i<n; i++)	
	{
		beta = householder_vector(v, &(A[(i*m)+i]), m-i);

		for (d = i; d < n; d++) {
			for (c = i; c < m; c++) {
				vAux[c] = 0;
	        	for (k = i; k < m; k++) 
	        	{
	        		if (c!=k)
	        		{
	        			aux = -beta * v[c-i] * v[k-i];
	        		}
	        		else
	        		{
	        			aux = 1 + -beta * v[c-i] * v[k-i];
	        		}
	        		
	        		vAux[c] = vAux[c] + aux * A[k+m*d];
	        	}
	      	}
	      	for (c=i; c<m; c++)
	      		A[c+m*d] = vAux[c];
	    }
	}

	/**
	*	Hacer backsustitution sobre la matriz triangular superior
	*/

	int rows = n-1;

	step[rows-1] = A[rows*m+(rows-1)] / A[(rows-1)*m+(rows-1)];

	for (i=rows-2; i>=0; i--){
		step[i] = A[i+m*rows];

		for (j=i+1; j<rows; j++){
			step[i] -= A[j*m + i] * step[j];
		}
		step[i] = step[i] / A[i+m*i];
	}
	
}

/**
*	Podrian haber overflows?? La version de matlab tambien admite el overflow aqui
*/
__cudaFunction__
real sumSquares(real *fvec, int nObs)
{
	real sum = 0.0;
	int i;

	for (i=0; i<nObs; i++)
	{
		sum += fvec[i] * fvec[i];
	}

	return sum;
}

/**
*	J'*costFun
*	El resultado es un array de tres elementos
*/
__cudaFunction__
void calcularGradF(real *gradF, real *fjac, real *fvec, int nObs, int nPrm)
{
	int i,j;
	real sum;

	for (i=0; i<nPrm; i++){
		sum = 0;
		for (j=0; j<nObs; j++){
			sum = sum + fjac[i*nObs+j] * fvec[j];
		}
		gradF[i] = sum;
	}
}

__cudaFunction__
void computeGradF(real *fgrad, real *fjac, real *fvec, int nObs, int nPrm)
{
    int i, j;
    real sum;
    
    for (i=0; i<nPrm; i++){
        sum = 0;
        for (j=0; j<nObs; j++){
            sum = sum + fjac[i*nObs+j] * fvec[j];
        }
        fgrad[i] = sum;
    }
}

__cudaFunction__
void levmar(real *x, real *xdata, real *ydata, real *residual, int nObs)
{
	int i;

	/**
	*	Variables auxiliares para el qrsolver
	*	(Intentar reducir el numero....)
	*/

	real qrSolverVAux[NUM_MAX_NOBS+numParams];
	real qrSolverV[NUM_MAX_NOBS+numParams];
	real Aux[(NUM_MAX_NOBS+numParams)*(numParams+1)];	

	int nPrm = numParams;

	/**
	*	Iniciar variables dependientes del sistema
	*	Por ahora, solo definidas como estaban en matlab
	*	(Cambiar alguna de estas a las verdaderas)
	*/

	real tolX   = 1e-5;
	real tolFun = 1e-5;
	real tolOpt = 1e-4 * tolFun;

	real eps = 2.2204e-16;
	real sqrtEps = sqrt(eps);
    
    real gradF[numParams];
    
	/**
	*	Variables auxiliares
	*/

	real fjac[NUM_MAX_NOBS*numParams];
	real fvec[NUM_MAX_NOBS];

	real step[numParams];

	real trialX[numParams];
	real trialFvec[NUM_MAX_NOBS];

	real lambda = 0.01;
    
    int maxFunEvals = 1000;
	int maxIter  = 1000;
	int iters = 1;
    int numFunEvals = 1;
    
	real sumSq, trialSumSq;

	/**
	*	Condiciones de terminacion
	*/

	int successfulStep = TRUE;
	int done = FALSE;
    
    real relFactor;
            
	/**
	*	Iniciar algoritmo
	*/

	modelo(x, xdata, ydata, nObs, nPrm, fvec, fjac, RESIDUAL);
	sumSq = sumSquares(fvec, nObs);

	modelo(x, xdata, ydata, nObs, nPrm, fvec, fjac, JACOBIAN);
    
    computeGradF(gradF, fjac, fvec, nObs, nPrm);
    
    relFactor = max(norm(gradF,nObs),sqrtEps);

	while (!done) 
	{
		iters++;

		qrSolver(Aux, fjac, fvec, nObs+nPrm, nPrm+1, step, lambda, qrSolverVAux, qrSolverV);

		for (i=0; i<nPrm; i++)
			trialX[i] = x[i] + step[i];

		modelo(trialX, xdata, ydata, nObs, nPrm, trialFvec, fjac, RESIDUAL);
		trialSumSq = sumSquares(trialFvec, nObs);
		
        numFunEvals = numFunEvals + 1;
        
		if (trialSumSq < sumSq)
		{
			for (i=0; i<nObs; i++)
				fvec[i] = trialFvec[i];

			for (i=0; i<nPrm; i++)
				x[i] = trialX[i];

			if (successfulStep)
				lambda = 0.1*lambda;

			modelo(x, xdata, ydata, nObs, nPrm, fvec, fjac, JACOBIAN);
            numFunEvals = numFunEvals + 3;
            
			successfulStep = TRUE;
            
            computeGradF(gradF, fjac, fvec, nObs, nPrm);
            
            if (norm(gradF,nPrm) < tolOpt * relFactor)
            {
                // printf("0");
                done = TRUE;
            }
            else if (norm(step, nPrm) < tolX*(sqrtEps + norm(x,nPrm)))
			{
                // printf("1");
				done = TRUE;
			} 
			else if (absolute(trialSumSq - sumSq) <= tolFun*sumSq) 
			{
                // printf("2");
				done = TRUE;
			}
			else if (iters > maxIter)
			{
                // printf("3");
				done = TRUE;
			}

			sumSq = trialSumSq;
		}
		else
		{
			lambda = 10*lambda;
			successfulStep = FALSE;

			if (norm(step, nPrm) < tolX*(sqrtEps + norm(x,nPrm)))
			{
                // printf("4");
				done = TRUE;
			}
            else if (numFunEvals > maxFunEvals)
            {
                done = TRUE;
            }
		}
        
	}

	*(residual) = sumSq;

}

/**
*	Comprobar que el tipo de datos de la imagen (i.e. short, long, int)
*	es uno que el sistema puede procesar
*/

/**
*	Correspondencia entre el tipo de datos del nifti y como se hace el cast
*	DT_INT16: (short)
*/

const int numValidTypes = 1;
int validDatatypes[numValidTypes] = {DT_INT16};

int isValidDatatype(int datatype)
{
	int i;

	for (i=0; i<numValidTypes; i++){
		if (validDatatypes[i] == datatype){
			return TRUE;
		}
	}
	return FALSE;
}

/**
*	Clase de resultado para devolver valores del kernel
*/

typedef struct    
{
    real solution[numParams];
    real residual;
    int id;
} 
ResultType;

/**
*	Estructura de la imagen para pasar los datos al kernel (size..)
*/

struct ImageSize
{
	// dimensiones de la imagen
    int dx, dy, dz;
    // Numero total de voxels de la imagen (dx*dy*dz)
    int totVoxels;
    // Numero total de voxels de la imagen (dx*dy*dz)
    real bValues[NUM_MAX_NOBS];
    // Numero total de voxels de la imagen (dx*dy*dz)
    int obs;
    // Numero total de threads
	int totThreads;
	// Datatype
	int type;
} 
image;

/**
*	Convertir el pixel de la imagen a tipo 'real', cualquier sea el
*	tipo/formato de la imagen
*/

__cudaFunction__
real convertirPixel(int type, int indx, void* dataimage)
{	
	// short
	if (type == DT_INT16)
	{
		short value = ((short *)dataimage)[indx];
	   	return (real)value;
	}

	// raise error?
	// Se supone que esto se ha comrpobado antes..
	return 0.0;
}

__global__
void kernel_levmar(ResultType  pResults[], ImageSize img, void* dataimage)
{

	/**
	*	Convertir el indice del thread en una lista (xi, yi, zi)
	*/

	// int other = img.observations;

	int nPrm = numParams;
	int nObs = img.obs;

	const int threadId = (blockIdx.x * blockDim.x) + threadIdx.x;
	int localId;

	/**
	*	Haciendo la division con ceil(), tenemos que se pueden crear mas threads de los que queremos
	*	aunque esos threads no se salen de los limites de img.totVoxels. su posicion sera calculada
	*	despues por el thread correspondiente
	*/
	if (threadId > img.totThreads)
		return;

	for (localId=threadId; localId<img.totVoxels; localId+=img.totThreads)
	{
		int cmp = img.dx * img.dy;  // Rango completo de x*y

	    int zi  = localId / (cmp);
	    int lcl = localId - (cmp) * zi;    // quitar la profundidad

	    int xi  = lcl % img.dx;
	    int yi  = lcl / img.dy;

	    /**
	    *	A partir de las referencias (xi, yi, zi) obtener los valores
	    *	nObs valores de profundidad
	    */
	    
	    int i, indx, num_zeros = 0;

	    //	nObs se cambia por MAX_NOBS sino no compilara cuando se haga sin nObs constante
	    //	Ademas, dado que la imagen puede ser de muchos tipos (short, float, double)
	    //	ya no podriamos hacer bien el cambio en 'convertirPixel'
	    //	es como si dijeramos un formato unico
	    //	Imagina que decimos que ydata es siempre short. y en ese
	    
	    real ydata[NUM_MAX_NOBS];	// Ahora es short, se canviara eso? no lo cambies, manten siempre la misma
	    real value;	// short value;

	    for (i=0; i<nObs; i++)
	    {
	    	indx = xi + (img.dy*yi) + (img.dx*img.dy*zi) + (img.dx*img.dy*img.dz*i);
	    	
	    	// value = ((short *)dataimage)[indx];
	    	// ydata[i] = (real)value;

	    	value = convertirPixel(img.type, indx, dataimage);
	    	ydata[i] = value;

	    	if (value == 0.0)
	    		num_zeros = num_zeros + 1;
	    }

	   	/**
		*	Ejecutar el algoritmo levmar
		*/

	    // Condicion del programa. si al menos la mitad son ceros, la solucion es cero
	    // Esto venia determinado por al algoritmi DWI
	    if (num_zeros > 3)
	    {
	    	for (i=0; i<nPrm; i++) 
	        {
	            pResults[localId].solution[i] = 0;
	        }
	        pResults[localId].residual = 0;
	    }
	    else
	    {
			real x[numParams];
			initialize_params(x);

			// real xdata[6] = {0.0, 50.0, 100.0, 200.0, 500.0, 1000.0};
			real *xdata = img.bValues;

			real residual = 0;

			levmar(x, xdata, ydata, &residual, nObs);
			
			// Pasar valores
			for (i=0; i<nPrm; i++)
			{
				pResults[localId].solution[i] = x[i];
			}
			pResults[localId].residual = residual;
		}
	}

	// pResults[localId].residual = some;
}	

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

const int NUM_THREADS_PER_BLOCK = 150;

/*
*	Numero maximo de threads que se van a ejecutar. Si:
*	numeroVoxels < NUM_MAX_THREADS: Se ejecutaran 'numeroVoxels' threads y cada uno computa un voxel
*	numeroVoxels > NUM_MAX_THREADS: Se ejecutan NUM_MAX_THREADS threads y cada uno intenta ejecutar
*				   (numeroVoxels/NUM_MAX_THREADS) voxels.
*/

const int NUM_MAX_THREADS = 500000;

int main(int argc, char **argv)
{
	/**
	*	Read nitfi file
	*/
	
	int nPrm = numParams;
	int i;

	if (argc == 1) 
	{
		fprintf(stderr, "** No tienes archivo de entrada \n"); return 2;
	}

	nifti_image * nim=NULL;
	char * fin = argv[1];

	//	leer desde linea de comandos los b values
	if (argc == 2)
	{
		fprintf(stderr, "** No tienes valores para bvalues \n"); return 2;
	}
	int numBvalues = atoi(argv[2]);

	if (argc != 3 + numBvalues)
	{
		fprintf(stderr, "** No hay suficientes bvalues. Esperados %d, encontrados %d\n", numBvalues, argc-3); return 2;
	}
	// leer los valores, pasalos directamente a la referencia de img que se ira al kernel
	for (i=0; i<numBvalues; i++)
	{
		image.bValues[i] = atoi(argv[3+i]);
	}

	printf("BValues\n");
	for (i=0; i<numBvalues; i++)
	{
		printf("%f ", image.bValues[i]);
	}
	printf("\n");

	nim = nifti_image_read(fin, 1);
	if (!nim) 
    {
        fprintf(stderr,"** failed to read NIfTI image from '%s'\n", fin); return 2;
    }

    printf("# Archivo nifti correcto '%s'\n", fin);

    /**
    *	Extraer informacion del archivo (i.e numero voxels, tipo de datos...)
    */

    int totalVoxels = 1;

    printf("# Dimensiones: \n");

    // Estos parametros se pueden acceder con otros formatos en el struct
    // del nifti. por ahora lo dejo asi
    // Leer los voxeles, se evita la ultima columna, porque es la 'profundidad'
    for (i = 0; i<nim->ndim-1; i++)
    {
    	printf("\t %d\n", nim->dim[i+1]);
        totalVoxels = totalVoxels * nim->dim[i+1];
    }
    
    int numObservaciones = nim->dim[nim->ndim];
    int imageTotalSize = totalVoxels * numObservaciones;	// Tamanno total de la imagen

    printf("# Numero maximo de threads: %d\n", NUM_MAX_THREADS);
    printf("# Voxels totales: %d\n", totalVoxels);
    printf("# Numero observaciones: %d\n", numObservaciones);
    printf("# Datatype %d\n", nim->datatype);
    printf("# Datatype (String) %s\n", nifti_datatype_string(nim->datatype));
    printf("# Numero de incognitas %d\n", numParams);

    if (numObservaciones != numBvalues)
    {
		fprintf(stderr,"** Hay %d imagenes y %d bvalues. Los valores deben coincidir \n", numObservaciones, numBvalues); return 2;
    }

    // Datos de la imagen para el kernel
    image.dx = nim->dim[1];
    image.dy = nim->dim[2];
    image.dz = nim->dim[3];

    image.type = nim->datatype;
    image.obs = numObservaciones;

    /**
    *	Comrpobar el tipo de los datos
    */

    if (isValidDatatype(nim->datatype)==FALSE)
    {
    	fprintf(stderr,"** El tipo de datos de la imagen '%s' no se puede procesar (Sin implementar) \n", nifti_datatype_string(nim->datatype)); return 2;
	}

    /**
    *	Mover imagen nifti a la memoria de cuda (TODO: el short ixe esta mal)
    */

    void * dataimage = 0;

    unsigned long int numeroBytesTotalesImage = imageTotalSize * nim->nbyper;

    printf("# Numero de bytes de la imagen %lu\n", numeroBytesTotalesImage);

    gpuErrchk( cudaMalloc(&dataimage,  numeroBytesTotalesImage) );
    gpuErrchk( cudaMemcpy(dataimage,nim->data, numeroBytesTotalesImage,cudaMemcpyHostToDevice) );

    // gpuErrchk( cudaMalloc(&dataimage,  imageTotalSize * sizeof(short)) );
    // gpuErrchk( cudaMemcpy(dataimage,nim->data, imageTotalSize * sizeof(short),cudaMemcpyHostToDevice) );

    /**
    *	Calcular threads
    */

    int NUM_THREADS;

    if (NUM_MAX_THREADS > totalVoxels)
    {
    	NUM_THREADS = totalVoxels;   // -> numVoxels (192, 192, 12)
    }
   	else
   	{
   		NUM_THREADS = NUM_MAX_THREADS;
   	}

   	/**
   	*	Si se hace la division normal y el resto de la division no es cero, se hace round
   	*	entonces tendriamos que bloques*threadsporbloque es menor que el numero total de voxels
   	*	por eso se necesita hacer ceil() y lanzar algunos mas de los necesarios, para mantener el mismo
   	*	numero de threads por cada bloque
   	*	- Division para round: q = (x+y-1)/y;
   	*/

    int NUM_BLOCKS = (NUM_THREADS + NUM_THREADS_PER_BLOCK - 1) / NUM_THREADS_PER_BLOCK;
    
    image.totVoxels = totalVoxels;
    image.totThreads = NUM_THREADS;

    printf("# Numero de threads: %d\n", NUM_THREADS);
    printf("# (Bloques, Threads) => (%d, %d)\n", NUM_BLOCKS, NUM_THREADS_PER_BLOCK);
    
    /**
    *	Clase para almacenar los resulados
    */

    ResultType * results_GPU = 0;
    gpuErrchk( cudaMalloc( &results_GPU,  totalVoxels * sizeof(ResultType)) );

    ResultType * results_CPU = 0;
    gpuErrchk( cudaMallocHost( &results_CPU, totalVoxels * sizeof(ResultType)) );

    /**
    *	Medir el tiempo de ejecucion
    */

    cudaEvent_t start, stop;
    float time;

    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start, 0);

	/**
	*	Ejecutar 
	*/

	printf("Kernel running...");
    kernel_levmar<<<NUM_BLOCKS,NUM_THREADS_PER_BLOCK>>> (results_GPU, image, dataimage);
    
    cudaThreadSynchronize();

    printf("GPU Done!\n");

    // Para el timer y mostrar

    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&time, start, stop);
	printf("Time for the kernel: %f ms\n", time);

    /**
    *	Copiar valores desde memoria en GPU a resultados CPU
    */

    gpuErrchk( cudaMemcpy(results_CPU, results_GPU, totalVoxels * sizeof(ResultType),cudaMemcpyDeviceToHost) );

    // una salida mas
    // 
    
    for (i=0; i<totalVoxels; i++)
    {
        for (int j=0; j<nPrm; j++)  
        {
        	printf("%f ", results_CPU[i].solution[j]);
        }
        printf("\n");
    }
	
	/**
	*	Liberar memoria en CPU, GPU y la imagen nifti
	*/

	gpuErrchk(cudaFree(dataimage));
    gpuErrchk(cudaFree(results_GPU));
    gpuErrchk(cudaFreeHost(results_CPU));

    cudaThreadExit();

    nifti_image_free( nim );
}
