#	Cuda Solver

## 	Solver

El código principal es el de solver.cu, se compila de la siguiente manera:

nvcc solver.cu -I./include -L./lib -lniftiio -lznz -lz -lm

Se necesitan las dependencias de include y lib para leer los archivos nifti de la imagen.

##	CheckNifti

Solver hace un preproceso en el que saca metadatos de la imagen (tamaño, tipo dato...), este codigo saca esa información también pero sin ejecutar el código. Se usa para comprobar la imagen y saber si es correcta según lo que solver necesita.

##	Uniform Solver

Hace que los threads se envíen de forma uniforme y así intentar reducir la carga. Por el momento parece que funciona en ciertas situaciones
