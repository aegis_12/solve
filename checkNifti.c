
#include <stdio.h>
#include <stdlib.h>
#include <nifti1_io.h>

/**
*	Comprobar el archivo nifti
*	Es lo mismo que hace solver.cu pero sin ejecutar todo el cuda
*/

int main(int argc, char **argv)
{
	/**
	*	Read nitfi file
	*/

	if (argc == 1) 
	{
		fprintf(stderr, "** No tienes archivo de entrada \n"); return 2;
	}

	nifti_image * nim=NULL;
	char * fin = argv[1];

	nim = nifti_image_read(fin, 1);
	if (!nim) 
    {
        fprintf(stderr,"** failed to read NIfTI image from '%s'\n", fin); return 2;
    }

    printf("# Archivo nifti correcto '%s'\n", fin);

    /**
    *	Extraer informacion del archivo (i.e numero voxels, tipo de datos...)
    */

    int i, totalVoxels = 1;

    printf("# Dimensiones: \n");

    // Estos parametros se pueden acceder con otros formatos en el struct
    // del nifti. por ahora lo dejo asi
    // Leer los voxeles, se evita la ultima columna, porque es la 'profundidad'
    for (i = 0; i<nim->ndim-1; i++)
    {
    	printf("\t %d\n", nim->dim[i+1]);
        totalVoxels = totalVoxels * nim->dim[i+1];
    }
    
    int numObservaciones = nim->dim[nim->ndim];
    int imageTotalSize = totalVoxels * numObservaciones;	// Tamanno total de la imagen

    printf("# Voxels totales: %d\n", totalVoxels);
    printf("# Numero observaciones: %d\n", numObservaciones);
    printf("# Datatype %d\n", nim->datatype);
    printf("# Datatype (String) %s\n", nifti_datatype_string(nim->datatype));

}